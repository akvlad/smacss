(function($, smaccs){

    function dataToggleEvent() {
        return function(){
            var dataToggle = $(this).attr('data-toggle');
            var dataTarget = $(this).attr('data-target') || false;

            if (dataTarget) {
                $(dataTarget).slideToggle();
            }
            else {
                $(this).slideToggle();
            }
        }
    }

    function initializeEvents() {
        $("[data-toggle]").on("click", dataToggleEvent());
    }

    smacss.dropdown = {
        init: function() {
            initializeEvents();
        }
    }
})(jQuery, window.smacss = window.smacss || {});
